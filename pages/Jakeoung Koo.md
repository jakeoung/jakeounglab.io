- Hello there, I'm Jakeoung Koo (구자경). My first name may be pronounced as Ja-Kyung.
	- Contact information: {firstname}@gmail.com or [Twitter](https://twitter.com/JaKeoungKoo) message
- ### Employment and education
	- Research Associate at Heriot-Watt University (Advisors: [Steve Mclaughlin](https://researchportal.hw.ac.uk/en/persons/stephen-mclaughlin), [Abderrahim Halimi](https://sites.google.com/site/abderrahimhalimi/))
	- Ph.D. at Technical University of Denmark in 2021 (Advisors: [Vedrana, A. Dahl](http://people.compute.dtu.dk/vand/), [Anders B. Dahl](http://qim.compute.dtu.dk/team/))
	- M.Eng. at Chung-Ang University in 2017 (Advisor: [Byung-Woo Hong](http://image.cau.ac.kr))
-
### News
	- A paper is submitted, entitled "A Bayesian Based Deep Unrolling Algorithm for Single-Photon Lidar Systems" with Steve and Abderrahim
	- I updated the cone beam geometry to simulate the forward projection of a triangle mesh for tomography. Check [the repository](https://github.com/jakeoung/shapefromprojections)
-
### Selected publication [(google scholar)](https://scholar.google.com/citations?user=LicWx04AAAAJ&hl=en)
	- Shape from projections via differentiable forward projector for computed tomography. J Koo, AB Dahl, JA Bærentzen, Q Chen, S Bals, VA Dahl. Ultramicroscopy, 2021.
	  [preprint](https://arxiv.org/pdf/2006.16120.pdf), [code](https://github.com/jakeoung/shapefromprojections), #tomography #rasterization
	  ![image.png](../assets/image_1631816477500_0.png){:height 193, :width 354}
	- DALM, Deformable Attenuation-Labeled Mesh for Tomographic Reconstruction and Segmentation. J Koo, AB Dahl, VA Dahl. IEEE Transactions on Computational Imaging, 2021. #[[tomography]] #[[topology-adaptive-mesh-deformation]]
-
### Misc
	- [[Useful textbooks or materials]]