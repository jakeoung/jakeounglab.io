## Textbooks I enjoyed or I am enjoying
	- Math or probability
		- Stanely Chan - Introduction to probability for data science (the best textbook for probability 101 with enough motivation and proofs with intuitions)
		- Ryu - [Large-Scale Convex Optimization via Monotone Operators](https://large-scale-book.mathopt.com)  (Focusing on monotone operators with its interplay with convex optimization in Euclidean spaces with different metrics)
			- Baushcke - Convex Analysis and Monotone Operator Theory in Hilbert Spaces (More comprehensive book and great for a reference. Many propositions are stated in a Hilbert space setting.)
		- Axler - Linear algebra done (finite dimensional vector space)
			- Axler - Measure, integration and real analysis (§2.A was really helpful to see why a measure is introduced. I like a simplified version of Zorn's lemma)
		- John M. Lee - Introduction to topological manifolds (smooth, Riemannian manifolds) (with proper motivation and clear notations)
	- Computational imaging or vision
		- Buzug - Computed tomography
		- Ma et al. - An Invitation to 3-D Vision: From Images to Geometric Models
	- To check
		- Hamilton - graph representation learning (only for a given graph)
		- https://probml.github.io/pml-book/book2.html
		- Introduction to statistical learning https://www.statlearning.com
		- Topological Data Analysis with Applications https://www.cambridge.org/core/books/topological-data-analysis-with-applications/00B93B496EBB97FB6E7A9CA0176F0E12
		- The_Bayesian_Choice_From_Decision_Theoretic_Foundations_to_Computational_Implementation).
		- Chung - Spectral Graph Theory
		- exploratory data analysis using fisher information
-
## Lectures
	- Daniel Cremers - Variational methods for computer vision (Youtube)
	- Keenan crane - Discrete differential geometry
	- Luxburg - [Statistical machine learning](https://www.youtube.com/watch?v=vCQmmeeSuuQ&list=PL05umP7R6ij2XCvrRzLokX6EoHWaGA2cC&index=38)
	- To check
		- CS224W Machine Learning with Graphs
		- https://github.com/sleglaive/BayesianML
		- NYU deep learning https://atcold.github.io/pytorch-Deep-Learning/
-
## Tutorials
	- Peyre - https://www.numerical-tours.com
	- statistics on manifolds https://geomstats.github.io/notebooks/01_data_on_manifolds.html
	- https://www.scratchapixel.com Best online material for getting started on computer graphics
-
## Misc
	- http://proximity-operator.net collects useful materials for proximal operators