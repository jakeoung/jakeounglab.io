- Infimal convolution may not be a familiar object, but it seems interesting. Here, I want to reinterpret some objects and concepts in convex analysis, using this infimal convolution.
## Infimal convolution
- **Def.** For any two functions $f,g : \mathcal H \to \mathbb R$, the infimal convolution of $f$ and $g$ is defined as
-
  $$f \square g : x \mapsto \inf _{y \in \mathcal{H}}(f(y)+g(x-y))$$
- **Properties.**
	- If $f,g$ are convex, so is $f\square g$.
	- For two functions $f,g: \mathcal H \to ]-\infty,+\infty]$, $(f \square g)^{*}=f^{*}+g^{*}$
-
## Reinterpretation
- $d_{C}=\iota_{C} \square\|\cdot\|$ is convex
-
-