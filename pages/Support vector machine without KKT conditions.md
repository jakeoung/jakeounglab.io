- The KKT condition is a great recipe for deriving dual problems for convex optimization, but I feel it sometimes obscures
- We consider the following optimization problem known as hard-margin linear classification:
-
  $$\min_{w,w_0} \frac{1}{2} \|w\|^2 \text{ s.t. } 1 - y_i ( \langle w, x_i \rangle + w_0) \leq 0$$
-
-
- Reference
	- https://dohmatob.github.io/research/2019/10/31/duality.html